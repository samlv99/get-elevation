const mbxClient = require("@mapbox/mapbox-sdk"); // Assuming this is the correct import path
const mbxTilesets = require("@mapbox/mapbox-sdk/services/tilesets");
const turf = require("@turf/turf");

// Replace with your actual Mapbox access token
const baseClient = "YOUR_MAPBOX_ACCESS_TOKEN";

async function getAverageElevation(lineString) {
  // Split the line into segments (e.g., 1 km)
  const chunks = turf.lineChunk(lineString, 1000).features;

  // Get elevation for each segment's starting point
  const elevations = await Promise.all(
    chunks.map(async (feature) => {
      const lngLat = feature.geometry.coordinates[0];
      const baseClient = mbxClient({
        accessToken:
          "pk.eyJ1IjoicHFqY2xhZG1pbiIsImEiOiJjbGZuZnlyMW0wNThqM3lzMGY0NDRtOTZzIn0.oG1UVzEbdIsrwspHgv_Dlw",
      });
      const tilesetsService = mbxTilesets(baseClient);
      const response = await tilesetsService.getElevation([lngLat]);
      return response.features[0].properties.elevation; // Access elevation property
    })
  );

  // Calculate and return the average elevation
  const totalElevation = elevations.reduce((acc, curr) => acc + curr, 0);
  const averageElevation = totalElevation / elevations.length;
  return averageElevation;
}

// Example usage
const lineString = {
  type: "Feature",
  geometry: {
    type: "LineString",
    coordinates: [
      [-71.328053, 44.313497],
      [-71.12971, 44.25279],
    ],
  },
  properties: {},
};

(async () => {
  try {
    const averageElevation = await getAverageElevation(lineString);
    console.log(`Average elevation along line: ${averageElevation} meters`);
  } catch (error) {
    console.error(error);
  }
})();
