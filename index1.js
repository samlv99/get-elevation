const polyline = require("@mapbox/polyline");
const fetch = require("node-fetch");

async function getElevationFromPolyline(encodedPolyline) {
  const accessToken =
    "pk.eyJ1IjoicHFqY2xhZG1pbiIsImEiOiJjbGZuZnlyMW0wNThqM3lzMGY0NDRtOTZzIn0.oG1UVzEbdIsrwspHgv_Dlw";
  const url = `https://api.mapbox.com/v4/mapbox.mapbox-terrain-v2/tilequery/${encodedPolyline}.json?access_token=${accessToken}`;
  // `https://api.mapbox.com/v4/mapbox.mapbox-terrain-v2/tilequery/${coordinates}.json?layers=contour&access_token=${accessToken}`;

  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(`HTTP error! status: ${response.status}`);
  }
  const data = await response.json();

  if (data.code !== "Ok" || !data.routes || data.routes.length === 0) {
    throw new Error("No routes found or unable to process the request");
  }

  // Extract elevations from the route geometry (GeoJSON)
  const elevations = data.routes[0].geometry.coordinates.map(
    (coord) => coord[2]
  );
  return elevations;
}

function averageElevation(elevations) {
  const sum = elevations.reduce((acc, ele) => acc + ele, 0);
  return sum / elevations.length;
}

async function getAverageElevation(points) {
  const encodedPolyline = polyline.encode(points);
  console.log("encodedPolyline", encodedPolyline);

  const elevations = await getElevationFromPolyline(encodedPolyline);

  return averageElevation(elevations);
}

// Ví dụ sử dụng
const points = [
  [-122.420679, 37.772537],
  [-122.421204, 37.772765],
  [-122.421291, 37.772781],
  // Thêm nhiều điểm nếu cần thiết
];

console.log("points", points);

getAverageElevation(points)
  .then((avgElevation) => {
    console.log("Độ cao trung bình:", avgElevation);
  })
  .catch((err) => {
    console.error("Lỗi:", err.message);
  });
